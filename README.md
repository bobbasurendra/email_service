#EmailService:-#

[![CircleCI](https://circleci.com/bb/bobbasurendra/email_service.svg?style=svg)](https://circleci.com/bb/bobbasurendra/email_service)

[![codecov](https://codecov.io/bb/bobbasurendra/email_service/branch/master/graph/badge.svg)](https://codecov.io/bb/bobbasurendra/email_service)



A service that accepts the necessary information and sends emails. It provides an abstraction 
between two different email service providers. If one of the services goes down,the service can quickly failover
to a different provider without affecting your customers.

#### Focus:-

* Backend focus
* Doesn't have a frontend web page. I provide curl request to the service.

### Features ###

* Supports 2 Email providers(Sendgrid and Mailgun).
* Email provider failover incase of failure of a service.
* In case of all provider failures, will just mark mail status as failed(In future can add a queuing service to retry after some time).
* Store the state of the provider, if a provider fails, it won't be used again until it is made active again(To make a failed provider active, should have a service which keeps checks and updates the state)
* Real time email sending(Scheduling is not supported for now).
* Simple Mail format only supported:-
    * content type:- Only Text format supported for now
    * single Recipient:- Can now only send to a single recipient
* Once email is queued on the email provider side, partial support for tracking queued emails(For now the tracker just updates the status to success)

### Technology Choices And Architecture ###

#### Technologies used:-

* scala - 2.12
* Akka - Maintaining state and manager senders/providers and failing over to another provider when one of them fails. And also used for tracking queued emails.
* Scalatra - A Simple web framework(I am using it for the first time). As the api is simple with no frontent, it is easier to start with.
* Mongodb - Persistence of email request and for generating unique id to send back to the user. As there are no relations in the data model, I used nosql db as it is easier to scale mongo and also because of high write throughput.
* Jetty - Recommended web server for scalatra framework
* CircleCI and Codecov- Circleci Continuous Integration site for build pipeline. Whenever code push is made, all testcases are run and pushes coverage reports. Codecov is for analysing coverage reports. Both have status badges in the readme.

#### Architecture

![Architecture](/images/architecture.png)

![Flowchart](/images/flowchart.png)

*Components*

* EmailServiceApi - Scalatra. Provides a post request api for sending email content. Once request is received, it will store the request in mongo with a uuid and forward the email data to MailerActor. Also provides a get request api for querying the email request status

* MailerActor - Akka actor service. An actor for managing list of senders and their availability. Has a default sender and will switch in case of failure. If all senders are not available, it will update the status of email request as failed. If sender was able to succesfully queue the request, then it will send the queuing message data to tracker for tracking the final delivery status.

* MailTrackerActor - Akka actor service. Will use the tracking data to track the queued email. For now marks it as delivered without querying the sender service

* ServiceChecker(Not implemented) - This would have activated failed senders/providers by periodically checking if the service is working or not. Didn't implement as this would need some way of testing whether service is active or not.

#### Architecuture Reasoning ####

* Needed a queuing service as the response shouldn't wait until one of the sender is succesful, which is inbuilt supported by akka. And also needed to manage the state of the senders/providers. And also if needed we can extract the akka service and make it distributed as well. And also easy to scale the actor service as well as very lightweight. Can use concurrency to increase the processing of the mailer service.
* For mongo choice, there is no relational data here, hence used mongo for persistence as well as to scale it up as well.
* Didn't implement full tracking, I had issues with tracking emails, as the some providers are giving webhooks instead of api querying, but plan to implement with given time.
* Didn't implement many features of emails such as bcc, cc, multiple recipients etc because as I chose to fine grain service reliability.
* Testing - Unit testing on senders/providers and functional testing of scalatra apis. Tested the available apis and also tested the senders/providers code. The tests missing are akka mailer and tracking actors. I mocked the behaviour in apis. Continuous integration and codecoverage are enabled for git pushes.

##### Libraries Used:-

* Jetty - webserver
* scalaLogging - For logging purposes, internally uses logback, log level is in DEBUG mode
* casbah - Mongo connector for scala
* configfactory - config loader library for scala
* json4s - json parsing library
* scalaj - http synchronous client
* scalatest, scalatratest - A test suite for functional testing of api and unit testing

##### Alternative tools I would have Used:-

* Akka Streams - I don't have experience and knowledge about it, but creating workflows and DAGs is builtin to akka streams. Would have simplified the whole top level architecture.

#### Scalability:-

How to scale the system?

In the current application, all the components are glued together in a single application except the db.

* High Availability:-
    To achieve high availability, we can deploy multiple instances of the application with db as a separate component. There is no issue(I think) for deploying multiple instances(I have one concern about UUID - as I generate uuid on the app, there could be a cause of collision, but this can be avoided by using objectId of mongo)
    For db availability, we can use mongo replica cluster.
    One problem with all components in single app is, the code could get too complicated and difficult maintain. And also difficult to find out how concurrency to allocate to each component.
* High Consistency:-
    This cannot be guaranteed on the db level because of replication, hence status data which is frequently viewed can be read wrong.
    Another issue of consistency where created record in primary is not present is replica, we won't have this issue as all the components are in a single application and we share the mongo connection between components.
* Partitioning:-
    If data can't sit on one machine of db, we have to partition. As this is mostly write heavy, old data is mostly rendered useless, we could also delete the old data and use only one machine. If we are going by paritioning data, we can use consistent hashing to parition or range partitioning.
* Reliable Mail Delivery:-
    Failover of email providers and also queuing incase of exceptions or no provider availability.

* Scaling Architecture:-

![Scaling Architecture](/images/scaling_app.png)


### Service Access And Testing ###

The jar is hosted in google cloud compute and the service can be accesed by the curl below. Service is available on public ip http://35.198.130.62:8080

* Available Apis:
http://35.198.130.62:8080/ - Post request for email with email content
http://35.198.130.62:8080/<EMAILID> - Get request to track the email request status using the email id from the post request

```
curl -X POST \
  http://35.198.130.62:8080 \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
	"email": {
		"from": "bobba.surendra@gmail.com",
		"to": "bobba.surendra@gmail.com",
		"subject": "text",
		"body": "hello hola"
	}
}'
```

### Files of Note ###

These files have the important parts of the application

* https://bitbucket.org/bobbasurendra/email_service/src/master/src/main/scala/com/uber/EmailService.scala
* https://bitbucket.org/bobbasurendra/email_service/src/master/src/main/scala/com/uber/MailerActor.scala
* https://bitbucket.org/bobbasurendra/email_service/src/master/src/main/scala/com/uber/senders/Mailgun.scala
* https://bitbucket.org/bobbasurendra/email_service/src/master/src/main/scala/com/uber/senders/Sendgrid.scala
* https://bitbucket.org/bobbasurendra/email_service/src/master/src/main/scala/com/uber/MailTrackerActor.scala

#### Resume ####
https://drive.google.com/file/d/1EM6GqPWwLxVmCZvsAmEbyYO6zMsAigZP/view?usp=sharing

github profile: github.com/domitian

### How do I get set up? ###

* dependencies:
    * scala 2.12
    * sbt
    * mongodb

* Compiling:- `sbt assembly`

* For building deploy jar:- see http://scalatra.org/guides/2.5/deployment/standalone.html

* For running locally. 
    * `sbt`
    * `jetty:start`

* To run tests.
    * `sbt test`

* Default port used is 8080.

* Env variables:- 
    * SCALA_ENV -> development, production(prod settings are not there in application.conf)
    * PORT

* Curl request to test the service

```
curl -X POST \
  http://localhost:8080 \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
	"email": {
		"from": "bobba.surendra@gmail.com",
		"to": "bobba.surendra@gmail.com",
		"subject": "text",
		"body": "hello hola"
	}
}'

This will return an id to track your email request status.

Use this id to make a curl request to get the status like below

curl -X POST \
  http://localhost:8080/46d8420e-265a-4f3e-bc04-316ab60150d4 \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json'
```

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact