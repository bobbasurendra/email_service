name := "email-service"

version := "1.0"

scalaVersion := "2.12.6"

lazy val akkaVersion = "2.5.12"
lazy val ScalatraVersion = "2.6.+"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
  "org.scalaj" %% "scalaj-http" % "2.4.0",
  "org.scalatra"            %% "scalatra"          % ScalatraVersion,
  "org.scalatra"            %% "scalatra-scalate"  % ScalatraVersion,
  "org.scalatra"            %% "scalatra-specs2"   % ScalatraVersion    % "test",
  "ch.qos.logback" % "logback-classic" % "1.2.3" % "runtime",
  "com.typesafe" % "config" % "1.3.2",
  "org.eclipse.jetty"       %  "jetty-webapp"      % "9.4.7.v20170914" % "container;compile",
  "javax.servlet"           %  "javax.servlet-api" % "3.1.0" % "provided",
  "org.scalatra" %% "scalatra-json" % ScalatraVersion,
  "org.json4s"   %% "json4s-jackson" % "3.5.0",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0",
  "org.mongodb" %% "casbah" % "3.1.1",
  "org.json4s" %% "json4s-mongo" % "3.5.4",
  "org.scalatra" %% "scalatra-scalatest" % ScalatraVersion % "test",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test"
)
enablePlugins(ScalatraPlugin)
