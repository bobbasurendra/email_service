package com.uber.senders
import Sender._
import scalaj.http._
import com.uber.{EmailData, EmailTrackerData}

import org.scalatest.FunSpec
import com.uber.UnitSpec

import scala.concurrent.duration._
import scala.language.postfixOps


class SendgridSpec extends UnitSpec{

  def buildDummyObject ={
      new Sendgrid("http://localhost", "/message", "bearerToken", "b@b.com")
  }

  describe("sendgrid processResponse method") {
  	it("should return QUEUED and tracker data for http response success code of 202"){
      val responseBody = """{"id": "abcd", "message": "request is queued"}"""
      val responseCode = 202
      val messageId = "abcd"
      val responseHeaders = Map("X-Message-Id" -> IndexedSeq("abcd"))
      val httpResponse = HttpResponse(responseBody, responseCode, responseHeaders)
      val sendgrid = buildDummyObject
	  sendgrid.processResponse(httpResponse) shouldEqual ((QUEUED, responseBody, Some(EmailTrackerData(messageId, "sendgrid"))))
	}

    it("should return CHANGE_SENDER_AND_RETRY and no tracking for 5xx http code"){
      val responseBody = "Internal error"
      var responseCode = 500
      val responseHeaders = Map[String,IndexedSeq[String]]()
      val httpResponse = HttpResponse(responseBody, responseCode, responseHeaders)
      val sendgrid = buildDummyObject
	  sendgrid.processResponse(httpResponse) shouldEqual ((CHANGE_SENDER_AND_RETRY, "Server issues", None))
      responseCode = 502
      sendgrid.processResponse(httpResponse.copy(code=responseCode)) shouldEqual ((CHANGE_SENDER_AND_RETRY, "Server issues", None))
      responseCode = 404
      sendgrid.processResponse(httpResponse.copy(code=responseCode)) shouldEqual ((CHANGE_SENDER_AND_RETRY, "Server issues", None))

    }

    it("should return RETRY without changing default sender and no tracking for 400 http code"){
      val responseBody = "Bad request"
      val responseCode = 400
      val responseHeaders = Map[String,IndexedSeq[String]]()
      val httpResponse = HttpResponse(responseBody, responseCode, responseHeaders)
      val sendgrid = buildDummyObject
	  sendgrid.processResponse(httpResponse) shouldEqual ((RETRY, "Bad Request", None))
    }
  }

}