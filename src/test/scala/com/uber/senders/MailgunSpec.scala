package com.uber.senders
import Sender._
import scalaj.http._
import com.uber.{EmailData, EmailTrackerData}

import org.scalatest.FunSpec
import com.uber.UnitSpec

import scala.concurrent.duration._
import scala.language.postfixOps


class MailgunSpec extends UnitSpec{

  def buildDummyObject() ={
      new Mailgun("http://localhost", "/message", "username", "apiKey", "b@b.com")
  }

  describe("Mailgun processResponse method") {
  	it("should return QUEUED and tracker data for http response success code"){
      val responseBody = """{"id": "abcd", "message": "request is queued"}"""
      val responseCode = 200
      val responseHeaders = Map[String,IndexedSeq[String]]()
      val httpResponse = HttpResponse(responseBody, responseCode, responseHeaders)
      val mailgun = buildDummyObject
	  mailgun.processResponse(httpResponse) shouldEqual ((QUEUED, responseBody, Some(EmailTrackerData("abcd", "mailgun"))))
	}

    it("should return CHANGE_SENDER_AND_RETRY and no tracking for 5xx http code"){
      val responseBody = "Internal error"
      var responseCode = 500
      val responseHeaders = Map[String,IndexedSeq[String]]()
      val httpResponse = HttpResponse(responseBody, responseCode, responseHeaders)
      val mailgun = buildDummyObject
	    mailgun.processResponse(httpResponse) shouldEqual ((CHANGE_SENDER_AND_RETRY, "Server issues", None))
      // For code 502
      responseCode = 502
      mailgun.processResponse(httpResponse.copy(code=responseCode)) shouldEqual ((CHANGE_SENDER_AND_RETRY, "Server issues", None))
      responseCode = 404
      mailgun.processResponse(httpResponse.copy(code=responseCode)) shouldEqual ((CHANGE_SENDER_AND_RETRY, "Server issues", None))

    }

    it("should return RETRY without changing default sender and no tracking for 400 http code"){
      val responseBody = "Bad request"
      val responseCode = 400
      val responseHeaders = Map[String,IndexedSeq[String]]()
      val httpResponse = HttpResponse(responseBody, responseCode, responseHeaders)
      val mailgun = buildDummyObject
	    mailgun.processResponse(httpResponse) shouldEqual ((RETRY, "Bad Request", None))
    }
  }

}