//#full-example
package com.uber

import org.scalatest.{ BeforeAndAfterAll, WordSpecLike, Matchers }
import akka.actor.ActorSystem
import akka.testkit.{ TestKit, TestProbe }
import scala.concurrent.duration._
import org.scalatra.test.scalatest._
import com.mongodb.casbah.Imports._
import org.scalatest.FunSuiteLike
import scala.language.postfixOps
import akka.testkit.{ ImplicitSender, TestActors, TestKit }
import com.typesafe.config.ConfigFactory

//#test-classes
class EmailServiceSpec
  extends TestKit(ActorSystem("MySpec"))
  with ScalatraSuite 
  with Matchers
  with ImplicitSender
  with FunSuiteLike
  with BeforeAndAfterAll {

  val mailer = system.actorOf(TestActors.echoActorProps)
  val conf = ConfigFactory.load().getConfig("development")
	val mongoConf = conf.getConfig("mongo")
  val mongoClientURI = MongoClientURI(mongoConf.getString("mongoURI"))
  val mongoClient = MongoClient(mongoClientURI)
  val database = mongoClient(mongoConf.getString("database"))
  val coll = database(mongoConf.getString("collection"))


  override def afterAll {
    database.dropDatabase()
    mongoClient.close()
    TestKit.shutdownActorSystem(system)
  }



  addServlet(new EmailService(system, mailer, coll, conf ), "/*")
  test("a invaild mail request must return bad request code and valid request with success code") {

    val invalidBody = """{
	"emal": {
		"to": "bobba.surendra@gmail.com",
		"subject": "text",
		"body": "hello hola"
	} }"""
    val validBody = """{
	"email": {
		"to": "bobba.surendra@gmail.com",
		"subject": "text",
		"body": "hello hola"
	} }"""

    post("/", invalidBody) {
      status should equal (400)
    }

    post("/", validBody) {
      status should equal (200)
    }

  }

  test("Fetching invalid mail request id will return 404 not found error") {

    get("/dddd") {
      status should equal (404)
    }
  }
}
