package com.uber
import com.uber.senders._
import Sender._
import akka.actor.{ Actor, ActorLogging, ActorRef, ActorSystem, Props }
import scala.reflect.runtime.{universe => ru}
import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import com.typesafe.config._
import com.typesafe.scalalogging._
import com.mongodb.casbah.Imports._


object Mailer {
  def props(providerConf: List[ConfigObject], mongoConf: Config, tracker: ActorRef): Props = Props(new Mailer(providerConf, mongoConf, tracker))

  def getProviderSettings(providerList: List[ConfigObject]) = {
    for{
      provider: ConfigObject <- providerList
      entry <- provider.entrySet.asScala
      providerName = entry.getKey
      providerSettings = entry.getValue
    } yield (providerName -> providerSettings)
  }

  /*
  Create senders using sender conf
  Only supports two senders for now
  */
  def createSenders(providerList: List[ConfigObject]) = {
    val settingsMap = getProviderSettings(providerList)
    settingsMap.map {case (providerName, providerSettings) =>
      providerName match {
        case "mailgun" => ("mailgun", Mailgun.build(providerSettings))
        case "sendgrid" => ("sendgrid", Sendgrid.build(providerSettings))
      }
    }.toMap
  }

  /*
  Get a list of available senders in the senderMap.
  A sender availability is stored in the sender object itself
  */
  def getAvailableSenders(senderMap: Map[String, Sender]): ListBuffer[Sender] = {
    val availableSenders = senderMap.toList.filter {case (senderName, sender) =>
      sender.isAvailable == true
    }.map{case (senderName, sender) => sender}
    return availableSenders.to[ListBuffer]
  }

  /* 
  Sender which is not in listed senders
   */
  def getOneAvailableSender(senderMap: Map[String, Sender], usedSenders: ListBuffer[Sender]) = {
    val senderList = getAvailableSenders(senderMap)
    val availableSenders = senderList.filter(sender => !usedSenders.contains(sender))
    if (availableSenders.length == 0)
      throw new NoProviderException("No provider available")
    availableSenders.head
  }

}

class Mailer(providerConf: List[ConfigObject], mongoConf: Config, tracker: ActorRef) extends Actor with ActorLogging with LazyLogging {
  import Mailer._

  val mongoColl = Util.createMongoConnection(mongoConf)

  val senderMap = createSenders(providerConf)
  var defaultSender = getOneAvailableSender(senderMap, ListBuffer())
  // val currentSender =
  /*
  From a list of available senders, try each one to send mail, try until all senders are used. If it fails for all, the mail request is marked as failed.
  If mail request is succesful, it will be sent to mail tracker
  */ 
  def receive = {
    case emailData: EmailData => {
      val usedSenders = ListBuffer[Sender]()
      var currentSender = defaultSender
      var mailQueued = false
      try{
        while (!(mailQueued)){
          logger.debug(s"Sending mail using $currentSender")
          val (status, message, trackingData) = currentSender.sendMail(emailData)
          Util.updateEmailRequestStatus(mongoColl, status, emailData.id)
          status match {
            case QUEUED => 
              mailQueued = true
              tracker ! trackingData.get.copy(emailId=emailData.id)
            case RETRY => {
              usedSenders += currentSender
              currentSender = getOneAvailableSender(senderMap, usedSenders)
            }
            case CHANGE_SENDER_AND_RETRY => {
              usedSenders += currentSender
              currentSender.isAvailable = false
              currentSender = getOneAvailableSender(senderMap, usedSenders)
            }
          }
        }
      }
      catch {
          case e: NoProviderException => 
            logger.debug("No provider available")
            Util.updateEmailRequestStatus(mongoColl, FAILED, emailData.id)
      }
    }
  }


}