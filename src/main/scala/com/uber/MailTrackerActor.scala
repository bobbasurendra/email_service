package com.uber
import com.uber.senders._
import Sender._
import akka.actor.{ Actor, ActorLogging, ActorRef, ActorSystem, Props }
import scala.reflect.runtime.{universe => ru}
import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import com.typesafe.config._
import com.typesafe.scalalogging._
import com.mongodb.casbah.Imports._
import scala.concurrent.duration._

object MailTracker {
  def props(providerConf: List[ConfigObject], mongoConf: Config): Props = Props(new MailTracker(providerConf, mongoConf))

}

class MailTracker(providerConf: List[ConfigObject], mongoConf: Config) extends Actor with ActorLogging with LazyLogging {
  import Mailer._

  val mongoColl = Util.createMongoConnection(mongoConf)

  /*
  TODO - Implement functionality to support web hook tracking and also periodically poll the api of mail to get tracking data for messages
  For now marks the mail request as success
  */
  def receive = {
    case emailTrackData: EmailTrackerData => {
      logger.debug("------------")
      emailTrackData.sender match {
          case "mailgun" => {
              logger.debug(emailTrackData.toString)
              Util.updateEmailRequestStatus(mongoColl, SUCCESS, emailTrackData.emailId)
          }
          case "sendgrid" => {
            logger.debug(emailTrackData.toString)
            Util.updateEmailRequestStatus(mongoColl, SUCCESS, emailTrackData.emailId)
          }
      }
    }
  }


}