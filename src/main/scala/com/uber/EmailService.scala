//#full-example
package com.uber

import Util._
import com.uber.senders.Sender._

import java.util.UUID.randomUUID
import org.scalatra._
import org.scalatra.ActionResult._ 
// JSON-related libraries
import org.json4s.{DefaultFormats, Formats}
import org.json4s.JsonAST._
import akka.actor.{ Actor, ActorLogging, ActorRef, ActorSystem, Props }
import com.typesafe.config._
// JSON handling support from Scalatra
import org.scalatra.json._
import com.typesafe.scalalogging._

// MongoDb-specific imports
import com.mongodb.casbah.Imports._



class EmailService(system: ActorSystem, mailer: ActorRef, mongoColl: MongoCollection, conf: Config) extends  ScalatraServlet with JacksonJsonSupport with LazyLogging {
  protected implicit lazy val jsonFormats: Formats = DefaultFormats

  /*
  Read params and create emailData object, generate uuid and store it in mongo and send email request to mailer actor
  */
  post("/") {
    val jsonString = request.body
    logger.info(s"params are $jsonString")

    // Parse json
    try{
      val jValue = parse(jsonString)
      val emailJValue = jValue \ "email"
      if (emailJValue == JNothing)
        halt(400, Map("message"->"Invalid input data"))
      val emailData = emailJValue.extract[EmailData]

      //generate uuid
      val id = randomUUID().toString
      // Forward to Maileractor
      mailer ! emailData.copy(id=id)
      // Save in mongo
      val newObj = MongoDBObject("status" -> "pending", "emailId" -> id, "emailData" -> emailData)
      mongoColl += newObj
      Map("id" ->id)
    } catch {
      case e: org.json4s.package$MappingException => halt(400, Map("message"->"Invalid input data"))
    }
  }

  get("/:emailId") {
    val emailId = params("emailId")
    val q = MongoDBObject("emailId" -> emailId)
    val records = for ( x <- mongoColl.findOne(q) ) yield x
    if (records.size == 0)
      halt(404, "email request not found")
    logger.debug("-----")
    val status = records.get("status").asInstanceOf[Long] match {
      case SUCCESS => "Success"
      case FAILED => "Failed"
      case QUEUED => "Queued"
      case _ => "Pending"
    }
    Map("status"-> status)
  }

  get("/retry/:emailId") {
    val emailId = params("emailId")
    val q = MongoDBObject("emailId" -> emailId)
    val records = for ( x <- mongoColl.findOne(q) ) yield x
    if (records.size == 0)
      halt(404, "email request not found")
    logger.debug("-----")
    val status = records.get("status").asInstanceOf[Long] match {
      case SUCCESS => "Success"
      case FAILED => "Failed"
      case QUEUED => "Queued"
      case RETRY => {
        val emailData = records.get.getAs[EmailData]("emailData") orElse ( halt(500, Map("message"->s"Data corruption in email request $emailId")) )
        mailer ! emailData
        Util.updateEmailRequestStatus(mongoColl, RETRY, emailId)
      }
      case _ => "Pending"
    }
    Map("status"-> status)
  }
}


