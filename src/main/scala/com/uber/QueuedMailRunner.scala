package com.uber
import com.uber.senders._
import Sender._
import akka.actor.{ Actor, ActorLogging, ActorRef, ActorSystem, Props }
import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import com.typesafe.config._
import com.typesafe.scalalogging._
import com.mongodb.casbah.Imports._
import akka.actor.Actor
import akka.actor.Props
import scala.concurrent.duration._
import java.util.Calendar
import scalaj.http._


class QueuedMailRunner(system: ActorSystem, mongoConf: Config, emailServiceApiConf: Config) extends LazyLogging {
  import Mailer._
	import system.dispatcher
  val mongoColl = Util.createMongoConnection(mongoConf)
  val serviceApiBasePath = emailServiceApiConf.getString("uri")
  
  /*
  Check for all mail which has to be sent before now and send request to email service to retry
  */
  def checkForQueuedMail() = {
    val now = Calendar.getInstance()
    for (queueMailRequest <- mongoColl.findAndRemove("sendAt" $lt now.getTime() )){
      val emailId = queueMailRequest.getAs[String]("emailId")
      val headers = Seq(("content-type", "application/json"))
      val fullPath = serviceApiBasePath + s"/retry/$emailId"
      val response = Http(fullPath).headers(headers).asString
      logger.debug(s"response code is ${response.code}")

    }
  }


}