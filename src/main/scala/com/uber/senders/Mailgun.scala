package com.uber.senders

import Sender._
import scalaj.http._
import com.uber.{EmailData, EmailTrackerData}
import com.typesafe.config._
import scala.collection.JavaConverters._
import com.typesafe.scalalogging._
import org.json4s._
import org.json4s.jackson.JsonMethods._

case class MailgunResponse(id: String, message: String)
object Mailgun extends LazyLogging{
  def build(settings: ConfigValue) = {
    val conf = settings.unwrapped.asInstanceOf[java.util.HashMap[String, String]].asScala
    val baseUrl = conf.getOrElse("baseUrl", "")
    val relativePath = conf.getOrElse("relativePath", "")
    val username = conf.getOrElse("username", "")
    val apiKey = conf.getOrElse("apiKey", "")
    val fromAddr = conf.getOrElse("from", "")
    logger.debug(conf.toString)
    new Mailgun(baseUrl, relativePath, username, apiKey, fromAddr)
  }
}

class Mailgun(baseUrl: String, relativePath: String, username: String, apiKey: String, fromAddr: String, isAvailable: Boolean=true) extends Sender(isAvailable) with LazyLogging {
  protected implicit lazy val jsonFormats: Formats = DefaultFormats
  type T = HttpResponse[String]
  def sendMail(emailData: EmailData): (Int, String, Option[EmailTrackerData]) = {
    val emailDataMap = EmailData.getMailgunData(emailData)
    val customFrom = if (emailDataMap("from") != "") emailDataMap("from") else fromAddr
    val updatedEmailDataMap = emailDataMap  + ("from" -> customFrom)
    val fullPath = baseUrl + relativePath
    val response = Http(fullPath).auth(username, apiKey).postForm(updatedEmailDataMap.toSeq).asString
    processResponse(response)
  }

    /*
    Process 
    */
  def processResponse(response: T): (Int, String, Option[EmailTrackerData]) = {
    logger.debug(response.toString)
    logger.debug(response.body)
    response.code match{
      case 200 =>
        val responseJson = parse(response.body).extract[MailgunResponse]
          return (QUEUED, response.body, Some(EmailTrackerData(responseJson.id, "mailgun")))
      case 400 =>
          // Bad request - maybe missing a parameter for this provider, so try another provider... but in this case it is probably a bad/invalid email address
        return (RETRY, "Bad Request", None)
      case 401 =>
          // Unauthorized, so api key expired, mark provider as not available.
        return (CHANGE_SENDER_AND_RETRY, "Unauthorized or api key expired", None)
      case _ =>
          // Other cases of error with mailgun server, so have to mark provider as not available here as well.
        return (CHANGE_SENDER_AND_RETRY, "Server issues", None)
    }

  }
}
