package com.uber.senders
import com.uber.{EmailData, EmailTrackerData}
abstract class Sender(var isAvailable: Boolean){
  type T
  def sendMail(emailData: EmailData): (Int, String, Option[EmailTrackerData])
  def processResponse(response: T): (Int, String, Option[EmailTrackerData])
}

object Sender {
  val SUCCESS = 1
  val FAILED = 2
  val PENDING = 3
  val QUEUED = 4
  val RETRY = 5
  val CHANGE_SENDER_AND_RETRY = 6
  val INVALID_MAIL_CONTENT = 7

  final case class NoProviderException(val message: String) extends Exception(message)

}