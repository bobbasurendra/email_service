package com.uber.senders

import Sender._
import scalaj.http._
import com.uber.{EmailData, EmailTrackerData}
import com.typesafe.config._
import scala.collection.JavaConverters._
import com.typesafe.scalalogging._

object Sendgrid extends LazyLogging{
  def build(settings: ConfigValue) = {
    val conf = settings.unwrapped.asInstanceOf[java.util.HashMap[String, String]].asScala
    val baseUrl = conf.getOrElse("baseUrl", "")
    val relativePath = conf.getOrElse("relativePath", "")
    val bearerToken = conf.getOrElse("bearerToken", "")
    val fromAddr = conf.getOrElse("from", "")
    logger.debug(conf.toString)
    new Sendgrid(baseUrl, relativePath, bearerToken, fromAddr)
  }
}

class Sendgrid(baseUrl: String, relativePath: String, bearerToken: String, fromAddr: String, isAvailable: Boolean=true) extends Sender(isAvailable) with LazyLogging {


  type T = HttpResponse[String]
  def sendMail(emailData: EmailData): (Int, String, Option[EmailTrackerData]) = {
    val emailDataMap = EmailData.getSendgridData(emailData)
    val fullPath = baseUrl + relativePath
    val headers = Seq(("Authorization", s"Bearer $bearerToken"), ("content-type", "application/json"))
    logger.debug(emailDataMap.toString)
    logger.debug(headers.toString)
    val response = Http(fullPath).headers(headers).postData(emailDataMap).asString
    processResponse(response)
  }

  def processResponse(response: T): (Int, String, Option[EmailTrackerData]) = {
    logger.debug(response.toString)
    logger.debug(response.body)
    response.code match{
      case 202 =>
        logger.debug(response.body)
        logger.debug(response.headers.toString)
        val trackingId = response.headers("X-Message-Id").head
        return (QUEUED, response.body, Some(EmailTrackerData(trackingId, "sendgrid")))
      case 400 =>
        // Bad request - maybe missing a parameter for this provider, so try another provider... but in this case it is probably a bad/invalid email address
        return (RETRY, "Bad Request", None)
      case 401 =>
        // Unauthorized, so api key expired, mark provider as not available.
        return (CHANGE_SENDER_AND_RETRY, "Unauthorized or api key expired", None)
      case _ =>
        // Other cases of error with mailgun server, so have to mark provider as not available here as well.
        return (CHANGE_SENDER_AND_RETRY, "Server issues", None)
    }

  }
}
