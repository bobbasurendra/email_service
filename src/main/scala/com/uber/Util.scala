package com.uber

import org.json4s.jackson.Json
import org.json4s.DefaultFormats
import com.mongodb.casbah.Imports._
import com.typesafe.config._

object Util{


  def toJson(m: Map[String, Any]): String = {
    Json(DefaultFormats).write(m)
  }

  def createMongoConnection(mongoConf: Config) = {
    val mongoClientURI = MongoClientURI(mongoConf.getString("mongoURI"))
    val mongoClient = MongoClient(mongoClientURI)
    mongoClient(mongoConf.getString("database"))(mongoConf.getString("collection"))
  }

  def updateEmailRequestStatus(mongoColl: MongoCollection, status: Long, emailId: String) = {
    val query = MongoDBObject("emailId" -> emailId)
    val statusUpdate = MongoDBObject("$set" -> MongoDBObject("status" -> status))
    mongoColl.update(query, statusUpdate)
  }
}