package com.uber

import Util._
case class EmailDataRequest(email: Map[String, String])

case class EmailData(from: String="", to: String, subject: String, body: String, id: String="")

object EmailData{
  def getMailgunData(emailData: EmailData) = {
    Map("from" -> emailData.from, "to" -> emailData.to, "subject" -> emailData.subject, "text" -> emailData.body )
  }
  def getSendgridData(emailData: EmailData) = {
    toJson(Map("personalizations" -> List(Map("to" -> Seq(Map("email"-> emailData.to)))), "from" -> Map("email" -> emailData.from), "subject"-> emailData.subject, "content"-> List(Map("type"-> "text/plain", "value" -> emailData.body)) ))
  }
}
case class EmailTrackerData(trackingId: String, sender: String, emailId: String="")