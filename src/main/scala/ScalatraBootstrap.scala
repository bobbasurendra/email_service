import com.uber._
import org.scalatra._
import javax.servlet.ServletContext
import akka.actor.{ Actor, ActorLogging, ActorRef, ActorSystem, Props }
import com.typesafe.config.ConfigFactory
import scala.collection.JavaConverters._

class ScalatraBootstrap extends LifeCycle {
	val environment = if (sys.env.get("SCALA_ENV") != None) sys.env("SCALA_ENV") else "development"
	val system: ActorSystem = ActorSystem("emailService")
	val conf = ConfigFactory.load().getConfig("development")
	val mongoConf = conf.getConfig("mongo")
	val mongoQueueConf = conf.getConfig("queuedMongo")
	val mongoColl = Util.createMongoConnection(mongoConf)

	val providerConf = conf.getObjectList("providers").asScala.toList

	val tracker: ActorRef = system.actorOf(MailTracker.props(providerConf, mongoConf), "trackerActor")
	val mailer: ActorRef = system.actorOf(Mailer.props(providerConf, mongoConf, tracker), "mailerActor")

	override def init(context: ServletContext) {
		context.mount(new EmailService(system, mailer, mongoColl, conf), "/*")
	}

	override def destroy(context:ServletContext) {
		system.terminate()
	}
}